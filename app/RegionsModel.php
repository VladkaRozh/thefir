<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionsModel extends Model
{
    public $timestamps = false;
    protected $table = 'regions_table';

    protected $fillable
        = [
            'region', 'modul_1', 'modul_2', 'modul_3', 'modul_4', 'modul_5'
        ];

    static function getRegions()
    {
        return RegionsModel::get();
    }

    static function addRegion($request)
    {
        return RegionsModel::create([
            'region' => $request->region,
            'modul_1' => $request->modul_1,
            'modul_2' => $request['modul_2'],
            'modul_3' => $request['modul_3'],
            'modul_4' => $request['modul_4'],
            'modul_5' => $request['modul_5'],
        ]);
    }

    static function editRegion($id, $request)
    {
        return RegionsModel::where('id', $id)->update([
            'region' => $request->region,
            'modul_1' => $request->modul_1,
            'modul_2' => $request['modul_2'],
            'modul_3' => $request['modul_3'],
            'modul_4' => $request['modul_4'],
            'modul_5' => $request['modul_5'],
        ]);
    }

    static function deleteRegion($id)
    {
        RegionsModel::find($id)->delete();
    }
}
