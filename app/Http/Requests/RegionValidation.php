<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegionValidation extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'region'  => 'required',
            'modul_1' => 'required',
            'modul_2' => 'required',
            'modul_3' => 'required',
            'modul_4' => 'required',
            'modul_5' => 'required',
        ];
    }
}
