<?php

namespace App\Http\Controllers;

use App\RegionUsers;
use Illuminate\Http\Request;

class RegionUsersController extends Controller
{
    public function showUsers()
    {
        $users = RegionUsers::get();
        return view('admin_blades.users', compact('users'));
    }
}
