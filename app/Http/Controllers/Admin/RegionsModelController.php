<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegionValidation;
use App\RegionsModel;
use Illuminate\Http\Request;

class RegionsModelController extends Controller
{
    public function getRegions()
    {
        $regions = RegionsModel::paginate(10);
        $regions->withPath('/regions');
        return view('/admin_blades.regions', compact('regions'));
    }

    public function addRegion(RegionValidation $request)
    {
        RegionsModel::addRegion($request);
        return $this->getRegions();
    }

    public function editRegion($id)
    {
        if ($id != null) {
            $region = RegionsModel::find($id);
        }
        return view('admin_blades.admin_region_edit', compact('region'));
    }

    public function saveRegion(RegionValidation $request)
    {
        RegionsModel::editRegion($request['id'], $request);
        return $this->getRegions();
    }

    public function deleteRegion($id)
    {
        RegionsModel::deleteRegion($id);
        return $this->getRegions();
    }
}
