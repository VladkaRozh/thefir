<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionUsers extends Model
{
    protected $table = 'region_users_table';

    protected $fillable
        = [
            'login', 'password'
        ];


}

