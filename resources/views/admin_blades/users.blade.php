@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-body">Dashboard</div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Login/th>
                        <th>Password</th>
                        <th>Edit</th>
                        <th>Delete</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $s)
                        <tr>
                            <th scope="row">{{$s['id']}}</th>
                            <td>{{$s['login']}}</td>
                            <td>{{$s['password']}}</td>
                            <td><a class="btn btn-warning"
                                   href="/admin/editRegion/{{$s['id']}}"
                                   role="button">edit</a></td>
                            <td><a class="btn btn-danger"
                                   onclick="return confirm('Are you sure？')"
                                   href="/admin/delete/{{$s['id']}}"
                                   role="button">delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->links()}}
                <br>
                <a class="btn-link" href="/admin/addRegion">{{ __('Add new Region') }}</a>
                <p><a class="btn-link" href="/home">{{ __('Back') }}</a></p>
            </div>
        </div>
    </div>

@endsection