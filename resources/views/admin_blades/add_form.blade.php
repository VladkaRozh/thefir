@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div>
                    <br>
                    <form enctype="multipart/form-data"
                          id="form-ins"
                          method="post"
                          action="/admin/addRegion">
                        @csrf
                        <p><b>region:</b></p>
                        @foreach ($errors->get('region') as $message)
                            <p class="text-danger"><{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               type="text"
                               required
                               name="region"/>
                        <p><b>modul_1:</b></p>
                        @foreach ($errors->get('modul_1') as $message):?>
                        <p class="text-danger">{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               required
                               name="modul_1">
                        <br>
                        <p><b>modul_2</b></p>
                        @foreach ($errors->get('modul_2') as $message)
                            <p class="text-danger">{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               required
                               name="modul_2"/>
                        <br>
                        <p><b>modul_3</b></p>
                        @foreach ($errors->get('modul_3') as $message)
                            <p class="text-danger">{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               required
                               name="modul_3">
                        <br>
                        <p><b>modul_4</b></p>
                        @foreach ($errors->get('modul_4') as $message)
                            <p class="text-danger">{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               required
                               name="modul_4">
                        <br>
                        <p><b>modul_5</b></p>
                        @foreach ($errors->get('modul_5') as $message)
                            <p class="text-danger">{{$message}}</p>
                        @endforeach
                        <input class="input-ins"
                               required
                               name="modul_5">
                        <br>
                        <br>
                        <input type="submit"
                               name="submit"
                               class="btn btn-warning"
                               value="save">
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection