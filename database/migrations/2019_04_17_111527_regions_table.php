<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region');
            $table->string('modul_1');
            $table->string('modul_2');
            $table->string('modul_3');
            $table->string('modul_4');
            $table->string('modul_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions_table');
    }
}
