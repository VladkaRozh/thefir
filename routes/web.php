<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/regions', 'RegionsModelController@getRegions');
Route::get('admin/editRegion/{id}', 'RegionsModelController@editRegion');
Route::post('admin/editRegion/{id}', 'RegionsModelController@saveRegion');
Route::get('admin/addRegion', function () {
    return view('admin_blades.add_form');
});
Route::post('admin/addRegion', 'RegionsModelController@addRegion');
Route::get('admin/delete/{id}', 'RegionsModelController@deleteRegion');

Route::get('users','RegionUsersController@showUsers');